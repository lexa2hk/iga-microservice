package com.fintracker.backend.igamicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IgaMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(IgaMicroserviceApplication.class, args);
    }

}
