package com.fintracker.backend.igamicroservice.controller;

import com.fintracker.backend.igamicroservice.gigachat.GigachatApiConnector;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class GigachatController {
    GigachatApiConnector gigachatApiConnector;

    @GetMapping("/token")
    public String getToken() {

        return gigachatApiConnector.getToken();
    }
}
