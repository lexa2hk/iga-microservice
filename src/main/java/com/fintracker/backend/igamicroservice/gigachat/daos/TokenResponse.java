package com.fintracker.backend.igamicroservice.gigachat.daos;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@Entity
@ToString
public class TokenResponse {
    @Id
    public String access_token;
    public String expires_at;
}
