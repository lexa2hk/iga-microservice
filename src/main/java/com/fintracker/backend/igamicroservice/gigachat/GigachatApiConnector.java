package com.fintracker.backend.igamicroservice.gigachat;


import com.fintracker.backend.igamicroservice.gigachat.daos.TokenResponse;

import com.fintracker.backend.igamicroservice.gigachat.repository.TokenRepository;
import jakarta.annotation.PostConstruct;
import kong.unirest.core.MultipartBody;
import kong.unirest.core.Unirest;
import kong.unirest.core.UnirestException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import java.util.logging.Logger;

@Component
@RequiredArgsConstructor
@AllArgsConstructor
//@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GigachatApiConnector{

    private TokenRepository tokenRepository;
    private final String authUrl="https://ngw.devices.sberbank.ru:9443/api/v2/oauth";
    private final String baseUrl ="https://gigachat.devices.sberbank.ru/api/v1";
    private final String secretsBase64 = "NGYzMzMyMDUtMzZmMy00OTg2LWIwMzctMTg4MmVjZTM0NDRjOnNlY3JldA==";



    @PostConstruct
    public void init() {
        Unirest.config().verifySsl(false);
    }



    public String getToken() {
        String uuid = UUID.randomUUID().toString();

        TokenResponse tokenResponse = null;
        MultipartBody response = Unirest.post(authUrl)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Accept", "application/json")
                .header("RqUID", uuid)
                .header("Authorization", "Basic NGYzMzMyMDUtMzZmMy00OTg2LWIwMzctMTg4MmVjZTM0NDRjOjdjN2Q0ZTUxLTVhNDAtNDI1ZC1hMWQ4LTM4MWY4ZTVjNzljMQ==")
                .field("scope", "GIGACHAT_API_PERS");
        try {
            tokenResponse = response.asObject(TokenResponse.class).getBody();
            tokenRepository.save(tokenResponse);

        } catch (UnirestException e) {
            throw new RuntimeException(e);
        }

        return tokenResponse.getAccess_token();
    }
}
