package com.fintracker.backend.igamicroservice.gigachat.repository;

import com.fintracker.backend.igamicroservice.gigachat.daos.TokenResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TokenRepository extends JpaRepository<TokenResponse, String> {

    @Query("SELECT t FROM TokenResponse t WHERE t.expires_at > CURRENT_TIMESTAMP")
    String getLastValidToken();
}
